# Maintainers' Contract

## 0. PREAMBLE

Primarily based on trademark law, this Contract covers the uses of
Distinctive Marks (both registered and unregistered) where such uses are
carried out as an economic activity including any reputational gains
made by using the initiator's goodwill, or the delivery of improved
technical services to any beneficiary who may have requested specific
software enhancements or service improvements.

This Contract aims to be compatible with both FOSSmarks (available on
[*http://fossmarks.org/*](http://fossmarks.org/)) and Model Trademark
Guidelines (available on
[*http://modeltrademarkguidelines.org*](http://modeltrademarkguidelines.org))
projects. However, should any conflict in meaning arise, the provisions
of this Contract shall be interpreted in full favour of the rights it
grants here.

This Maintainers Contract (the "Contract") applies to any
Distinctive Mark as associated with the Programme and its Open Source
Licence, with no prejudice to the rights given on the Programme by its Open Source Licence.

This Maintainers' Contract also aims, by specifying it, to harmonise the international application of fair use, particularly about reference purposes.

## 1. DEFINITIONS
   
In this Contract, unless otherwise provided or the context otherwise
requires, capitalised expressions (where the single shall include the
plural and vice versa) shall have the meanings as identified below:

**"Contract"** This agreement between the Initiator and the Licencee,
which accepts the terms and conditions upon which the Licencee is given
the right to Use the Distinctive Mark

**"OSD"** Open Source Definition available at https://opensource.org/osd.

**"FSD"** Free Software Definition available at https://www.gnu.org/philosophy/free-sw.en.html.

**"Free and Open License"** The OSD or FSD compliant licence associated with the
Programme

**"Distinctive Mark"** The name and logo as usually associated with
the Program, the name of the copyright holder or its organisation; or
for the purposes of this contract, any other mark (name and/or logo
whether registered or unregistered), which has been expressly mentioned
as Distinctive Mark by the Initiator in Exhibit A (Distinctive Marks and
Trademark Policy)

**"Initiator"** The legal entity to which the Programme's support and
maintaince is attributed; or in the event where such entity does not or ceases to exist, the Programme's project leader as declared on the Programme's dedicated
website or software repository, and who is in charge of redistributing
the Programme's revenue

**"Programme"** The Programme licenced under a Free and Open Source Licence on which a Distinctive Mark is used and which is distributed in accordance with this Contract. 
The term Programme includes any future versions of such software as published by the
Initiator provided that the Contract remain attached to it

**"Use"** Use means exercising any exclusive rights as granted or
protected by intellectual property law, including copyright law,
trademark law and registered designs rights in relation to the Programme
and/or Distinctive Mark; or
reproducing, copying, publicly performing, publicly displaying the
Distinctive Mark in/on any medium, or associating the Distinctive Mark
to the Product or Associated Services

**"Licencee"** A Licencee may be any person or entity making use of
the Distinctive Mark ("**Commercial Intermediary**"); or
any person or entity who benefiting from such use ("**Beneficiary**")

**"Product"** The Programme or a software product based on or derived
from the Programme, or a combination of the Programme and other software
products, whose value derives entirely or substantially from the
functioning of the Programme; or any inclusion of the Programme in a 
commercial product offering

**"Trademark Policy"** Trademark Policy refers to publication by the
Initiator regarding the Distinctive Mark in which there is an explicit
reference to the Contract. All existing Trademark Policies must be
supplied under Exhibit A

For interpretation purposes, should there be provisions with conflicting
meanings across Trademark Policies, the latest version of the Trademark
Policy, as published on the Initiator's website, shall prevail

**"Associated Services"** Services whose value derive, entirely or
substantially, from the functionality of the Programme. Associated
Services include, but are not limited to the following services:
training, conference and workshop, design, commercial distributions and
warranties, hosting, merchandising, events, sponsoring or use of any
aforesaid services

**"Similar Mark"** Any mark with confusing similarity to a Distinctive
Mark for the use of which a permission is deemed necessary to avoid the
likelihood of confusion in the public.

## 2. **LEGAL SCOPE**

### 2.1  **EMBODIED AGREEMENTS AND LICENCES**

This Contract comprises the following agreements, each binding in its
own right:

* **2.1.1** A trademark licence
* **2.1.2** A copyright and neighbouring rights licence
* **2.1.3** A licence of any registered designs copyright as related to the Distinctive Mark

### 2.2 **INTELLECTUAL PROPERTY RIGHTS**

All intellectual property rights attached to, resulted from, caused by
or relevant to the Distinctive Mark are covered under this Contract. The
Contract also covers the following Distinctive Mark aspects:

#### 2.2.1  **TRADEMARK RIGHTS**

The parties to this Contract agree that whether unregistered or
registered nationally, regionally or internationally, the Distinctive
Mark will enjoy full trademark law protection. The trademark holder is
entitled to the exclusive right to the use of the registered trademark,
for the legal duration of the trademark registration.

#### 2.2.2  **COPYRIGHT AND NEIGHBOURING RIGHTS**

The parties to this Contract agree that any copyright or neighbouring
rights associated with the Distinctive Mark is fully protected by
copyright law. Where relevant, the licensed Distinctive Mark in this
Contract covers all individual elements of copyright and neighbouring
rights. The parties to this Contract intend it to include economic and
moral rights as granted by law and/or this Contract for original
works of authorship fixed in a tangible medium of expression.

#### 2.2.3 **DESIGN COPYRIGHT RIGHTS**

The parties to this Contract agree and recognise that
it includes a non-exclusive licence to any relevant registered design
that is held by, under the control of, or sub-licensable by the
Initiator, to the extent necessary to facilitate making use of the
rights granted under this Contract.

## 3. **GRANT OF RIGHTS**

Subject to the terms and conditions of this Contract and its Sections 2 and 4-7, the Initiator grants to the Licencee a license on all Intellectual Property Rights described in Section 2 in order to Use the Distinctive Mark in accordance with Section 5-12 including the rights to 
  * provide and supply third parties with Products and/or Associated Services for a fee or any other consideration, including without limitation hosting fees, consultation fees or charges for support services, related to the supply of Programmes, Products and/or Associated Services;
  * advertise, endorse and promote Products, Associated Services or any legal entity, in any medium (such as domain name or URL) in the business forum relevant to Product and/or Associated Services; and

In order to cover all Intellectual Property Rights described above, specific licenses are detailed below. The scope of their grants should be interpreted consistently.

### 3.1 **GRANT OF TRADEMARK LICENCE**

* **3.1.1**  Subject to the terms and conditions of this Contract and its Sections 2 and 4-7, and without prejudice to Sections 11 and 12, the Initiator hereby grants to the Licencee, which accepts, a worldwide, non-exclusive, non-transferable, non-sublicensable, terminable trademark licence to Use the Distinctive Mark on or in relation to Products or Associated Services.

* **3.1.2**  This trademark licence under this Contract grants the Licencee the right to make a commercial or non-commercial Use of the unmodified Distinctive Mark, on any medium, physical or online, in any country or jurisdiction.
* **3.1.3**  Subject to the Licencee's full compliance with Section 8, and without prejudice to Section 11 of this Contract, the Licencee is permitted to use the expression "Official Maintainer" in conjunction with the Distinctive Mark.

### 3.2 **GRANT OF COPYRIGHT LICENCE**

* **3.2.1** Subject to the terms and conditions of this Contract and its Sections 2 and 4-7, and without prejudice to Sections 11 and 12, the Initiator hereby grants to the Licencee, which accepts, a worldwide, non-exclusive, non-transferable, non-sublicensable, terminable copyright licence to Use the Distinctive Mark on or in relation to Products or Associated Services.
* **3.2.2** This copyright licence under this Contract grants the Licencee the right to license to reproduce or represent the Distinctive Mark.

### 3.3 **GRANT OF DESIGN COPYRIGHT LICENCE**

* **3.3.1** Subject to the terms and conditions of this Contract and its Sections 2 and 4-7, and without prejudice to Sections 11 and 12, the Initiator hereby grants to the Licencee, which accepts, a worldwide, non-exclusive, non-transferable, non-sublicensable, terminable copyright licence to Use the Distinctive Mark on or in relation to Products or Associated Services.
* **3.3.2** This design copyright licence under this Contract grants the Licencee the right to make, sell, offer for sale a Product or Associated Service while using the Distinctive Mark.


## 4. **LIMITATIONS**

### 4.1  **FREE AND OPEN SOURCE SOFTWARE**

Notwithstanding any other provision of this agreement, this Contract
does not relate to the Programme itself which remains available under
the Free and Open Source Licence. This Contract shall not be interpreted
in any way to limit the rights given by the Open Source Licence.

This Contract only applies to the use of the Trademark, with no prejudice to Fair use doctrine. 

Anyone who wishes to use the Program under the terms of the Free and Open Source License under which it is distributed is granted the right to remove any reference to the Program's Trademark from the source code and naming of files and components, so that they may exercise their rights under the Free and Open Source License without restriction. 

### 4.2  **PATENTS**

Notwithstanding any other provision of this agreement, this Contract
does not cover, license, permit the use of, or give access to any
patents owned by the Initiator.

### 4.3  **UNFAIR COMPETITION**

Notwithstanding any other provision of this agreement, this Contract
cannot be interpreted in any way that would create an unfair advantage
to any party. The Contract is not discriminatory and everybody should have 
the rights to use the Distinctive Mark in accordance to this Contract, either
to provide Associated Services (subject to Section 6) or to answer any 
call for tender requiring the  Commercial Intermediary to comply with 
the Contract (Subsection 7.3).

### 4.4  **SIMILAR MARK, OTHER PRODUCTS OR ASSOCIATED SERVICES**

Notwithstanding Sections 2 and 3, or any other provision of this
agreement, this Contract does not cover or permit the Use of Similar
Marks or the Use of Distinctive Marks on a modified Product or on any
products or services other than the Products and Associated Services set
forth in this Contract.

### 4.5  **DURATION AND FORMAT**

The Contract grants a licence for the duration of the legal rights covered as described in Subsection 2.2. To the extent possible in the relevant jurisdiction, the rights may be exercised in any medium or format whether now known or created in the future. This right explicitly includes commercial use, but do not exclude any field of commercial activity.

### 4.6  **MULTIPLE-LICENCING**

The right to Use the Distinctive Mark under different terms is reserved. In the event that the Distinctive Mark is multiple-licenced the Licencee has the choice of using alternative licences. These uses and their terms of use might be detailed within the Trademark Policy. The Initiator does not grant any licence for the use of Similar Mark. Such use is totally forbidden unless it results from the conditions detailed in Section 6.

### 4.7 **USE NOT RELATED TO THE PROGRAM**

The Licencee is not permitted to Use the Distinctive Mark or Similar Mark regarding products or services not related to the Program:
* in any way or form which attempts to unfairly or unjustifiably capitalise on the Initiator's goodwill or brand including without limitation an attempt to imply an endorsement that does not exist;
* as part of or as its corporate or trading name; or
* incorporate in its business.

For reference purposes only, such Use is permitted only to the extent needed to explain the link with the Programme. In this case, a weblink must be provided to offer recipients a link to the Initiator's webpage or the one(s) dedicated to the Programme or Product should there be one. For instance, the Licencee is permitted to say: "This Product is compatible with *Word Mark* Progamme." or "*System Management Tool* for *Programme Name*" or "*Programme Name*-Based *System Management Tool*."

## 5.  **LICENCE CONDITIONS**

* **5.1** If the Licencee want to benefit from the licence granted in Section 3, then Licencee must rigorously comply with the directions of the Initiator regarding the form and manner of the application of the Distinctive Mark, including the directions contained in the Trademark Policy.
* **5.2**  Additionally, the Licencee must not:
   * **5.2.1**  either directly or indirectly, assist any other person or entity to use the Distinctive Mark except as expressly permitted under this Contract; or
   * **5.2.2**  do or omit or suffer to be done any act that will or may impair any registration of the Distinctive Mark, or take any action to challenge or diminish the Initiator's intellectual property rights including without limitation the rights attached to the Distinctive Mark.
* **5.3**  The Licencee's failure to fulfill the aforesaid conditions constitutes a material breach of this Contract's licence terms

## 6. **PRIVATE USE**

Private Use of the Distinctive Mark is unconditionally permitted on a
royalty-free basis. Private Use covers all forms of internal use of the
Programme including Software as a Service Offering (SaaS Offering)
unless such activity is conducted as part of Associated Services
offering or exploitation.

## 7. **RESTRICTED DISTINCTIVE MARK USE**

In order to comply with this Contract, each Use of the Distinctive Mark
by the Licencee shall strictly be conducted as outlined below:

* **7.1**  Making use of the Distinctive Mark while distributing a Product will only be permitted if the Distinctive Mark is used unmodified so that the Initiator can be recognized.
* **7.2** Making use of the Distinctive Mark while offering and/or exploiting Associated Services will only be permitted if there is clear reference to the Initiator and the Programme. In this context, if the Programme is used in conjunction with other software(s), the Licencee must clearly specify these other Programme uses.
* **7.3** The Beneficiary is hereby expressly permitted to request any Associated Service and to request any person or entity to answer a call for tender. However, if the Beneficiary request an Official Maintainer, only Contract-compliant Commercial Intermediaries are permitted to answer such request. In such case, upon contract signing, the Beneficiary is under an obligation to ensure that the Commercial Intermediary they are dealing with is Contract-compliant.

## 8.  **OBLIGATIONS**

### 8.1 **ROYALTIES**

* **8.1.1** The Licencee (Commercial Intermediary) undertakes to share with the Initiator any revenue from the commercialisation of Associated Services or selling Product on the basis on 1% (one percent) of the financial value generated while exploiting the Associated Service or selling the Product.
* **8.1.2** If there are several Programmes licensed under this Contract, the Licencee shall not share more than 5% (five percent) of the whole revenue generated from exploiting the Associated Service and/or selling the Product associated with the Programmes in question.
* **8.1.3** Non-commercial Use of the Distinctive Mark is permitted without the obligation to share revenues with the Initiator being applicable only if there is no profit-making activity in such Use.
* **8.1.4** Given that the Commercial Intermediary is the party responsible for fulfilling the fiscal obligations as specified in this Section; the Beneficiary is entitled to opt to fulfill these obligations instead of the entity offering the Associated Services.
* **8.1.5** Any sum due shall be paid to the Initiator at the end of the calendar year in which revenues were generated, or by agreement no later than March 31st of the following year.
* **8.1.6** The sums payable under this Contract are exclusive of VAT, GST, sales taxes or any similar taxes or levies.
* **8.1.7** These sums must be paid free from any deduction or withholding unless required by law. In this case, the Licencee hereby accepts to pay the Initiator additional sums as an offset against the deducted sums, as requested by law. Additionally, the Licencee shall supply the Initiator with a written financial statement outlining the gross amount of royalties, all types of deductions and any sum paid, within ten calendar days of paying such deduction or withholding.

### 8.2  **SHARING INFORMATION**

* **8.2.1** Based on the Gregorian calendar year, the Licencee shall maintain proper records and keep ledgers in a recording fiscal and commercial details of the Licencee's activities as linked to the commercalisation of Associated Services or Products can be reasonably identified.
* **8.2.2** The Licencee must annually share no later than 31 March of the following year, an official sales and/or financial report (hereinafter \"Information\") declaring the total revenue as linked to the rights granted by this Contract. This Information can be either shared with the Initiator himself or published on a publicly available website, which can be either the Licencee\'s own website or website specially dedicated to this purpose. This Information must be consistent with public available information such as: Licencee\'s report; publicly available information of publicly traded companies; trade publications; trade and technology exhibitions; information gathered by professional and business associations or published on professional and business magazines; journals and publications concerning the relevant Product or Associated Services.
* **8.2.3** The Licencee (Commercial Intermediary or Beneficiary, where relevant) undertakes to keep the Initiator informed of any Use of the Distinctive Mark. The Licencee shall inform the Initiator of all Information concerning the revenue generated from the Associated Services provided by the Commercial Intermediary.
* **8.2.4** The Licencee accepts and undertakes to automatically supply this Information --as relevant to each Use-- to every Beneficiary who receives an offer from the Initiator to benefit from this Contract.

## 9.  **DISCLAIMER OF LIABILITY**

In no event and under no legal theory, whether in tort (including
negligence), contract, or otherwise, unless required by applicable law
or expressly mentioned in this Contract, shall the Initiator, its
owner(s), employee(s), subcontractor(s), freelancer(s) or any person,
even if advised of the possibility of such damages, be liable for any
loss, expense or damage, of any type or nature including direct,
indirect, incidental, special, exemplary, or consequential loss or
damages of any character (including albeit without limitation to lost
profits), however caused or arising from the Licencee\'s exercise of the
rights granted in this Contract, or as a result of this Contract or out
of the Use or inability to Use the Product.

## 10.  **INTELLECTUAL PROPERTY WARRANTIES, CLAIMS AND DISCLAIMERS**

* **10.1** The Initiator represents and warrants to the Licencee that the Product, Distinctive Mark and all the intellectual property rights as granted in this Contract are owned by, licensed to, or sublicensed to the Initiator by their owners or licence owners, where applicable. The Initiator also warrants that it has not and will not use or incorporate into Products any intellectual property of others without the party's prior written consent.
* **10.2** To the best of their knowledge, Initiators represent and warrant to the Licencee that neither the Product nor the Use of the Product infringes any patent or copyright, trade secret, trademark, or other proprietary rights of any third party. The Initiator will make best efforts to inform the Licencee of any third party\'s intellectual property rights known in any country.
* **10.3** The Licencee hereunder accepts additional responsibilities towards Beneficiaries, end users, business partners etc. While this Contract is intended to facilitate the commercial Use of the Programme, the Commercial Intermediary who distributes or commercialises the Product should do so in a manner which does not create potential liability for the Initiator.
    * **10.3.1** Therefore, the Commercial Intermediary agrees to defend and indemnify the Initiator against any losses, damages or costs (collectively "Losses") arising from claims, lawsuits or any other legal actions brought by a third party against the Initiator to the extent caused by the acts or omissions of such Commercial Intermediary in connection with its distribution of the Programme in a commercial product offering.
    * **10.3.2** Nothing in this Contract shall constitute any representation or warranty that any registration comprised in the Distinctive Mark is valid; any application comprised in the Distinctive Mark shall proceed to grant registration or, if granted, shall be valid; or the exercise by the Licencee of rights granted under this Contract will not infringe the rights of any person or entity.

## 11.  **CONTRACT TERMINATION**

All Licencee's rights granted under this Contract shall be void and
this Contract shall terminate if the Licencee fails to comply with any
of the material terms or conditions of this Contract and/or the Free and
Open Source Licence, unless such breach is rectified by the Lisensee
within a reasonable period of time after the Licencee becomes aware of
such non-compliance issue.

## 12.  **MISCELLANEOUS**
   
### 12.1  **SEVERABILITY**

If any provision of this Contract be held invalid, illegal or
unenforceable under applicable law by a court of competent jurisdiction,
such invalidity shall not affect the validity, operation or
enforceability of the remainder of the terms of this Contract. Rather,
and without further action by the parties hereto, the invalid, illegal,
or unenforceable provision shall be modified to the extent necessary so
that it is valid, legal, and enforceable.

### 12.2  **THIRD PARTIES**

All rights not expressly granted under this Contract are reserved.
Nothing in this Contract is intended to be enforceable by any entity not
a Licencee nor the Initiator. No third party or Beneficiary rights are
created under this Contract.

### 12.3  **INDEMNITY**

The Licencee covenants with the Initiator not to do or omit or suffer to
be done any act that will or may weaken, damage or be detrimental to the
Distinctive Mark or the reputation or goodwill associated with the
Distinctive Mark or the Initiator, or that may invalidate or jeopardise
any current or potential registration of the Distinctive Mark.

### 12.4   **TRADEMARK REGISTRATION**
* **12.4.1**  The Licencee shall not challenge, apply for, obtain or attempt to obtain, the registration of the Distinctive Mark under any classification for any goods or services.
* **12.4.2** The Licencee shall not apply for, or obtain or attempt to obtain, the registration of any trademark or service mark, under any classification, in any country or jurisdiction any mark or name that consists of, comprises, or is a Similar Mark.

### 12.5 INDEPENDENT CONTRACTORS**

Nothing in this Contract is intended to or shall be deemed or construed
by its parties or by any other entity to create an agency or any type of
partnership or joint venture between any of its parties, or authorise
any party to make or enter into any commitments for or on behalf of any
other party of this Contract.

### 12.6  **COMPETENCY AND CAPACITY**

The Licencee acknowledges its full comprehension and understanding of
the terms and conditions of this Contract. The Licencee also confirms it
is acting on its own behalf and not for the benefit of any other person
or entity.

## **Exhibit A: Distinctive Marks and Trademark Policy**

### *Initiator Instructions:*

* **A.**  *As stated in the Contract, the Initiator may insert here its complete Trademark Policy and/or Certification Policy, or at least the list of Distinctive Marks concerned by the Contract. The Initiator may add additional information on trademark ownership.*
* **B.**  *If it is neither possible for nor desirable by the Initiator to insert such notice in a particular file, then the Initiator may include the notice in a separate location, which can be entitled:  TRADEMARK, in a relevant directory where the Licencee or a recipient would be likely to look for such a notice.*

### **Exhibit B: IPR Claims**
