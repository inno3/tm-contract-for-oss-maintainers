#FAQ

#You're a project's maintainer or member willing to use the Maintainers' Contract

##Does the Maintainers' Contract really keep the code open source/free software?

Yes, because the Maintainers' Contract doesn’t cover the same legal object than free or open-source licences. 
Indeed, software’s source code is considered as a specific creative work and, as such, is protected by copyright and may be licensed under a Open Source license ; whereas the Maintainers' Contract apply not to the source code, but to a recognizable sign, design, or expression which identifies the software and wich is based on Trademark Right. 

![Schéma 1](images/Schéma IP Rights.png)

##Why is different to use trademark instead of copyright to monetize open source/free software?

The main difference is that trademark keeps the software Open Source and doesn't question users' freedoms. The modalities associated with the Maintainer' Contract only concern the commercial use of the project's image, and not of the source code which remains Open Source. 

![Schéma 3](images/Schéma IP Rights 3.png)

##Do you have an example model that works already? 

Among a lot of others (Symfony, ElasticSearch, Ubuntu, etc.) a good example of that kind of IP policy can be Redhat’s business model. Redhat develops open-source software and in the same time sells products or services linked to this open-source code. 

[RedHat Business Model](images/redhat_businessmodel.png) Dragos Manac CC-BY-SA 4.0

##Is it a Trademark Policy? How to distinguish it or articulate it with a Maintainers' Contract ?

Trademark Policy is a way to formalize the terms of use of trademarks, commonly used in FOSS projects. These policies share the common specificity to combine restrictions to ensure that their trademarks are meaningful as an indicator of origin and quality of the software (like a traditional trademark policy would do) with a large set of authorizations to allow free usages by the community (which is fundamental for FOSS projects). Sometimes, specific contracts are concluded between the editors and operators who want to commercially use the trademark. They are kept secret. 

The Maintainers' Contract standardize this purpose, as it aims to explore a trademark-based systemic approach with reciprocal commitments between the Open Source Project and trademark users, in order to have standard and public documents (just like FOSS licenses are) to define sharing modalities of the revenues associated to the exploitation of trademarks.
Hence, Trademark Policy and Maintainer’s Contract can be used together, *subject to some adaptations*. 

##Do I need to register a trademark? How much do I need to pay to register a trademark?

You do not have to register your Trademark, but by doing it you will ensure an higher level of protection to your Trademark. 
The registering process is a national process (except for the EU where you can registered for all memberstates at once, and in Africa where several organizations coexist), so you have to consider the geographical area you want a protection first.
You can contact WIPO to put in place an international registration fo your trademark, which cost depends on the number of countries you want to be registered in: https://www.wipo.int/madrid/en/

##Who can help me to pay for the trademark registration?

We currently don't know any structure willing to help you pay for your trademark registration. So, if you or your company are willing to help Open Source projects to protect their trademark and to use the Maintainers' Contract, please contact us. 

##What rights do I get once I register my trademark?

Depending on the country where protection is seeked, registration is not mandatory, but always strongly recommended. Indeed, a trademark registration will confer or strengthen* an exclusive right to use the registered trademark for the designation of named goods or services. This implies that the trademark can be exclusively used by its owner, or licensed to another party for use. 

##What are my rights once I declare a Maintainers’ Contract?

A Maintainers' Contract does not create new rights for the trademark owner, but allows others to use the trademark as long as they respect the trademark contract's conditions. 

##Does it slow down the adoption of my project ? When is it the best time to enforce the maintainer contract ?

The Maintainers' Contract only concerns the commercial use of the trademark, therefore it wouldn't slow down the adoption of the project, but only to the use of the trademark. 
And for operators willing to commercially use the trademark, the project must be known from them and more, wich won't be the case at the beggining, so he Maintainers' Contract won't slow down early adoption at all. 

##What happens if someone forks my project?

In software engineering, a project fork happens when developers take a copy of source code from one software package and start independent development on it, creating a distinct and separate piece of software. It is a risk shared by all the FOSS projects.
If a fork happens to your project, that just mean maintainer's of this fork will not benefit from the Maintainer's Contract, except if you merge their work to your project, which would make sense. 
Forking is a risk once you put a trademark on your open-source project, but it doesn't mean it's a bad thing. There are some exemples of beneficient co-existence between a trademarked project and a fork of it, as RHEL and CentOS. 

##Where does the money go?

The money must be directly collected by the project structure. If there isn't such, the leader of the project, or identified as such, will be in charge of collecting and sharing revenues from the Maintainers' Contract. 

##Don’t want to spend time on management, who can help me manage the business of managing partner relationships and trademark contract?

As it could be for the accumulation and anonimization of the received amounts, the management work of the project could be done by Code Maintainers' syndicate organized by organization like the Maintainers.org or  Open Source Maintainers contributions platform like Tidelift.

##How do I have to spend the money?

The idea of the Maintainer's Contract is that the money you collect thanks to the trademark policy mechanism should be used to finance maintainers' work, at the discretion of the project, as openly as possible.

##What is different from Linux/Eclipse Foundation model?

Linux and Eclipse Foundations are user-centric umbrella-foundations : they help you to grow in exchange for money or IP Rights, when Maintainer's Contract is a legal tool you can use to help your sustainability. 
This means that if your project joins one of these foundations, its trademark will be ceased to it and you won't even be able to use the Maintainers' Contract without its agreement. 

##What is different from OpenCollective? 

OpenCollective offers a service to you, when this Contract is brought to you service-free, its a tool for you to use. You can use a Maintainers' Contract while joining OpenCollective. 

#You're an operator willing to use a project's trademark subject to a Maintainers' Contract


##When the Maintainers' Contract implies commercial users pay for using the project's trademark ?

As a rule, you may have to abide to the Maintainers' Contract and pay for the use of the project's trademark when this use may let average people believe that there is a link between the project and your activity. 
For example you can't use the project's trademark as 
- a visible metatag, adword, or URL
- a domain name pointing to similar goos or services as the project's
- a hashtag made only of the project's trademark
- an identity on social networks or repositories
- a company's name
- an hypertext link
 You also can't use the project's trademark on your business proposition or CV or advertisment or in any way that can be considered as commercial use. 

##What are cost-free use of the trademark according to Maintainers's Contract ?

You can freely use the project's trademark as long as there is no commercial interest for you in it, or if your use enters the scope of Trademark Fair Use, as as provided by Section 7. 
Regarding a Trademark, Fair Use can be of two types : 
	* nominative use, to designate the very goods that are the subject of the mark; 
	* descriptive use for an object generally associated with the mark.
More specifically, a use of the Trademark is considered in scope of Trademark Fair Use if it complies with the following:
    1/ The product or service cannot be readily identified without using the trademark (e.g. trademark is descriptive of a person, place, or product attribute).
    2/ The user only uses as much of the mark as is necessary for the identification (e.g. the words but not the font or symbol).
    3/ The user does nothing to suggest sponsorship or endorsement by the trademark holder. This applies even if the nominative use is commercial, and the same test applies for metatags.
Nominative fair use of a mark may also occur within the context of comparative advertising. 


