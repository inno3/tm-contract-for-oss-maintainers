# Complementary Projects

* Regarding Trademark:
    * [](http://modeltrademarkguidelines.org/"Model Trademark Guidelines"):Model Trademark Guidelines (by Pamela Chestek)
* Regarding FOSS:
    * [Open Source Initiative](https://opensource.org/"To promote and protect open source software and communities...").
    * [Free Software Definition](https://www.gnu.org/philosophy/free-sw.en.html"What is free software?"]
