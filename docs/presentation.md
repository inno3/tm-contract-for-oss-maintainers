# Presentations we did

We started to talk about the Contract at some conferences in 2019.

We first had a presentation at [API Day Conference](https://www.apidays.co/conferences) in Paris, then we talk about the Maintainers' Contract at [EOLE](http://eolevent.eu/) in Marseille, where we discuss license's use with [CoopCycle](https://www.coopcycle.org), a nonprofit that defend [another way](https://wiki.coopcycle.org/en:license) to use license in order to retribute maintainers.

He are the last versions of the slides we used to support our presentation.

![Slide0](images/slides-page-0.png)
![Slide1](images/slides-page-1.png)
![Slide2](images/slides-page-2.png)
![Slide3](images/slides-page-3.png)
![Slide4](images/slides-page-4.png)
![Slide5](images/slides-page-5.png)
![Slide6](images/slides-page-6.png)
![Slide7](images/slides-page-7.png)
![Slide8](images/slides-page-8.png)
![Slide9](images/slides-page-9.png)
![Slide10](images/slides-page-10.png)
![Slide11](images/slides-page-11.png)
![Slide12](images/slides-page-12.png)
![Slide13](images/slides-page-13.png)
![Slide14](images/slides-page-14.png)
![Slide15](images/slides-page-15.png)
![Slide16](images/slides-page-16.png)
![Slide17](images/slides-page-17.png)
![Slide18](images/slides-page-18.png)
