# About us

The original idea and first draft of this Contract came by Mehdi Medjaoui (from [The Maintainers.org](http://themaintainers.org/)), Benjamin Jean & Camille Moulin (from [Inno³](https://inno3.fr)):

* [The Maintainers.org](http://themaintainers.org/) is a global research network interested in the concepts of maintenance, infrastructure, repair, and the myriad forms of labor and expertise that sustain our human-built world. Our members come from  a variety of backgrounds, including engineers and business leaders, academic historians and social scientists, government and non-profit agencies, artists, activists, coders, and more.
* [Inno³](https://inno3.fr) is an expert independent consulting firm specializing in Open Innovation, Open Data andOpen Source fields.

## Current contributors

* Mehdi Medjaoui, Mehdi Medjaoui is an entrepreneur in the API industry, cofounder of OAuth.io, and creator of APIDays Conferences.
* Benjamin Jean, Benjamin is CEO of [inno³](https://inno3.fr) and chairman of [« Open Law*, Le droit ouvert »](https://openlaw.fr). He teaches Intellectual Property law in several universities, he is Master conferences at Science Po, administrator of Framasoft, and co-founder of the annual conference cycle « [European Open source & Free Software Law Event](https://eolevent.eu) » (EOLE).
* Camille Moulin, OSS Expert at [inno³](https://inno3.fr)
* Vincent Bachelet, IP Lawyer at [inno³](https://inno3.fr), PhD student 

## Former contributors

* Laure Kassem, IP Lawyer

## Reviewers

This project was reviewed by several lawyers and FOSS activists from different contries.
