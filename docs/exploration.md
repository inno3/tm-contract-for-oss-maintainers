# Introduction

Free and Open Source software (FOSS) has prompted a major revolution in 
the field of software development where boundaries have been pushed to
new limits proving how indispensable FOSS has become in promoting
commercial development, institutional transparency and digital freedom
of world citizens today.

Nevertheless, despite the economic value FOSS generates to users, many
FOSS projects do not provide their maintainers enough revenue streams to
secure them a more sustainable channel of income, which would allow them
to continue to develop FOSS programmes in a more sustainable manner and
under improved terms and conditions.

In the light of this, many initiatives have been launched to address
this issue from different perspectives, including the legal ones where
new licenses have been created. Unfortunately, some of these licences
had major drawbacks, which helped create a damaging ambiguity to both
the notion and the practice of open-source software development allowing
FOSS programme initiators who come out of licences being treated more
favourably than FOSS programme maintainers.

Hence, the motivation behind this Contract is to introduce a fairer
revenue sharing scheme that is more balanced towards FOSS maintainers,
while respecting the integrity of the framework and practice in the FOSS
sphere. This Contract, which functions along with a Free and Open Source
licence, permits fairer sharing of the revenues generated by the
commercialisation of this software. In the same time, it aims at protecting 
consumers from predatory business practices, preserving fair competition and 
ultimately combining advantages of Free and Open Source schemes with 
automatic and non-discriminatory licencing practices.

- **The problem**: Open Source projects'Maintainers's Work is (most of the time) not properly rewarded while some third parties extract significant value of their work
- **The goal** : find a legal / contractual framework for a fairer distribution of the value
- **Non satisfying initiatives to address the same problem** : Commons Clause license (because it's just a proprietary license), and customized trademark licenses of OSS projects are not standardized to make it easy for Developers to use/enforce them and users to understand/apply them
- **What we want to explore**: a Trademark license, that would allow to still use OSI approved / FSF definitions compatible licenses for the projects, and that is easily understandable and shareable thus.

# Ideal scenarios

The general idea is to funnel to its maintainers a part of the value and/or revenues generated by **intermediaries** based on the commercialization and/or branding of the software.

## Chargeable use of the trademarked software

- IT consulting companies (SSII/ESN) that promote and sell services (or certification/trainings) based on that product (use case 1)
- 3rd party software vendors that promote and include the covered software in their product (e.g. IBM, Oracle, etc.) (use case 2)

## Non chargeable use of the trademarked software

- Every internal use of the software, including for SaaS offering (use case 3)

# Getting into the details

In order to validate the principle of the license and get inputs for its content, an estimation of the following points is necessary.

## General problem: international heterogeneity of trademark laws
{To be checked by Benjamin}
Paris and Madrid Conventions

## Triggering of the license

### For paying usages
- **Risk to address**: not triggering the obligations when it should: such problem can't be solved by license text, as the license text doesn't apply.

#### Use case 1 (services)
If a service company advertises its services on the covered software, it might not always constitute an infringement of the trademark.
For example, this is probably the case according to French law, [Article L713-6](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000028748203&cidTexte=LEGITEXT000006069414&dateTexte=20181119):

> L'enregistrement d'une marque ne fait pas obstacle à l'utilisation du même signe ou d'un signe similaire comme : \[...\]  
> b) Référence nécessaire pour indiquer la destination d'un produit ou d'un service, notamment en tant qu'accessoire ou pièce détachée, à condition qu'il n'y ait pas de confusion dans leur origine ;

Also, if a request for support on the covered software comes from the user issuing a call for tender, there would be even fewer matter to enforce a trademark license on this service.
Even worse, if the license was potentially efficient to force respondent to a call for tender to pay something to the project, then the product wouldn't be allowed to be explicitly mentioned in a public call for tender, as it would break the fair competition.

#### Use case 2 (embedding in software or hardware)

If the covered software is shipped "as is", i.e. branded, its distribution surely would be restricted.
One problem could be in the context of a library that is compiled: would there remain enough trademark elements?


#### For non-paying usages
- **Risk to address**: triggering the obligations when it should not. This can be solved by license text.
As long as the usage is internal, it seems difficult to invoke any control over trademark usage. We know that RedHat sales people disagree on this, maybe others.
{To be checked by BJE}
But if the SaaS is advertising it (like "This Foo service is powered by Bar Project"), it most likely requires a trademark usage authorization. If the idea is to allow this kind of usage for free, such advertising should be encouraged and not prohibited, as it would help build the visibility hence the strength of the mark.

**Solution**: Add a clause in the license that explicitly authorized it and gives guidelines to encourage it.

## Pricing

### Uniformity of rates
Should the license define the amount of the payments to the project?
It can be reasonably assumed that having a uniform pricing scheme would help the perception of the fairness of the whole process.
It could be relevant to make the price to pay a part of the license or at least of the project version of the license, in order to have a standard pricing scheme.
The initial idea was to ask paying users to "pay back 0.1% of revenue generated by the product to the maintainers' organization".

### How to measure the base revenue for payment?
The revenue generated by the product can be tricky to evaluate, as there is A tiered approach may offer a solution.
{See what was discussed with Mehdi}
In a flatter and more uniform approach, the base revenue for payment can be associated by the size of the company (number of employees), the total revenues of the company in different tiers.

In this last example, that could lead to something like "Less than 1$M revenue --> $X, between $1M and $100M $XX, over 100M --> $XXX"

### How to fix the ratio?
The ratio is difficult to find,. The lower and flatter, the wider the license model can spread.
 
### Code contribution as payment?
It can seem fair that a 3rd party company that is actually contributing to the project (be it code base or other) is not asked to pay the same thing.
It could even be considered that from a certain level, such 3rd party company should be incorporated in the maintenance team, hence actually.

On way to solve that is that if the company can prove that it spend more in contribution/maintenance on the project than the license fee, the company is then exonerated.
If the amount is below the full license fee, the license fee amount can be reduced by the amount spent in maintenance/contribution every year. (timesheets of employee contributing*employee earning, sponsoring)

### Anonymity

Enforcing payment can be painful for a team of code maintainers learning to manage an organization. This is why at the beginning and as long it works, this is a declarative process where companies themselves declare their dependencies to the project into their commercial products. To guarantee anonymity of the revenues of each product form companies, the amount they sent go directly to a corporate lawyer/accountant that will accumulate and anonymize all the received amounts. Like that, every company is ensured that nobody will know directly their financial results.
This is the role than can be done by Code Maintainers' syndicate organized by organization like the Maintainers.org or Open Source Maintainers contributions platform like Tidelift.

## Resilience to forks/derivative unbranded projects

As the trademark license will put additional constraints on the project's users, it is technically possible that it may encourage forking or the creation of a unbranded version.
The example of RHEL/CentOS show that the existence of an unbranded version of the project might be compatible and even benefit to the branded project ; one should be careful when generalizing from this very peculiar example, though.
The creation of a forked version would mean that there are actual 3rd party contributors active enough to maintain a different version: the way 3rd contributions are handled (see [above](#code-contribution-as-payment))

In its essence, this is a maintainers' license more than a founder/creator license. If a new project comes from a fork but has a community of maintainers and people using the software, it makes sense to rewards these for their maintenance work.


## Negative impact on the adoption of the project

### By the population expected to pay
The obligation to pay will surely play as a deterrent on the population expected to pay. But considering the current success of projects looking for monetization/sustainability, it will only affect the project with low value/awareness or marginally the most renowned and trendy projects.
Some projects already have trademark restrictions that can lead to payments or needed certifications (often for a fee) like the [Symphony project trademark rules](https://symfony.com/trademark) Symfony 

### By the population not expected ot pay
Many users are sensitive to the freedom of the code they use, and wouldn't accept to use code that isn't really free, even if it only excludes populations that they are not part of. It is important that the trademark license is perceived as something fair, according to its original intent.

## Incompatibility with downstream distribution
The first example that comes to mind is the problem between Mozilla and Debian, that led to the creation of [Iceweasel, Icedove, Iceowl and Iceape](https://en.wikipedia.org/wiki/Mozilla_software_rebranded_by_Debian), and that was [eventually solved](https://www.pcworld.com/article/3036509/linux/iceweasel-will-be-renamed-firefox-as-relations-between-debian-and-mozilla-thaw.html).

## Comparison with "classical schemes"
There are already business models out there based on trademark usage. Of course, there is Red Hat, but the business model is more complex (it combines trademark, support, distribution channels,etc.). The ones that are more closely relying on trademarks are certification / partnership programs, etc. Cf. Alfresco.

### Existing trademark policies

The project [Model Trademark Guidelines](http://modeltrademarkguidelines.org/) explores the subject and proposes different best practices

- Canonical / Ubuntu : [Trademark policy](https://www.ubuntu.com/legal/intellectual-property-policy). See [some discussion on LWN back in 2007](https://lwn.net/Articles/232003/)
- Symfony https://symfony.com/trademark : it is inspired by Ubuntu's one. It is directly linked to a partnership program
> Restricted use that requires a trademark license
    - Any commercial use including for any services related to Symfony such as providing training services, conference services, or design services (should you wish to provide such services, please contact us beforehand to explore Sensiolabs' Partnership Program);


## Examples of recent project where the Maintainer trademark license could have solved the issue (as keeping the project OSS while finding ways of monetization)

### RedisLabs (avoiding proprietary license like the the commons clause)
Redislabs is a company providing on premises and cloud version or Redis key value store database. They closed a $44 million series D VC round in 2017 and they hired recently the creator of Redis as an employee to work on the core of Redis, which is under BSD license.
Recently , they added to their cloud connectors previously under AGPL license an Apache 2.0 + a new license called the Commons Clause, which is not an open source software license as define by the OSI, as it restricts the usage of the software for commercial use.
In a blog post on Wednesday, Redis Labs co-founder and CTO Yiftach Shoolman justified the license shift by saying that cloud providers benefit from open-source software while giving nothing back.
They CTO stated about this change :
"Cloud providers have been taking advantage the open source community for years by selling (for hundreds of millions of dollars) cloud services based on open source code they didn’t develop," he said, pointing to widely adopted projects like Docker, Elasticsearch, Hadoop, Redis, Spark etc.  

"This discourages the community from investing in developing open source code, because any potential benefit goes to cloud providers rather than the code developers or their sponsors. (...)
However, today’s cloud providers have repeatedly violated this ethos by taking advantage of successful open source projects and repackaging them into competitive, proprietary service offerings. Cloud providers contribute very little (if anything) to those open source projects.
Instead, they use their monopolistic nature to derive hundreds of millions dollars in revenues from them. Already, this behavior has damaged open source communities and put some of the companies that support them out of business.
Redis is an example of this paradigm. Today, most cloud providers offer Redis as a managed service over their infrastructure and enjoy huge income from software that was not developed by them. Redis Labs is leading and financing the development of open source Redis and deserves to enjoy the fruits of these efforts.
Redis is and will always remain open source BSD license, but we decided to prevent cloud providers from creating managed services from certain add-ons on top of Redis (e.g. RediSearch, Redis Graph, ReJSON, Redis-ML, Rebloom). These are licensed under Apache 2.0 modified with Commons Clause.
They also explained why the AGPL license was not enough (...)
We initially licensed them under AGPL. However, later on we realized AGPL does not prevent cloud providers from creating managed services from our code. Furthermore, we got requests from developers working at large enterprises, to move from AGPL to a more permissive license, because the use of AGPL is against their company policies."
This created a huge backlash (you can follow the discussion on [Hacker News] (https://redislabs.com/community/licenses/) about the license change and on [Hacker News](https://news.ycombinator.com/item?id=17819652) about the Commons Clause).
Some influent software developers such as Chris Lamb, the Debian Linux project leader, and Nathan Scott, a Fedora developer decided to fork the RedisLabs projects like [Goodformcode] (https://goodformcode.com/) and they explained :

"With the recent licensing changes to several Redis Labs modules making them no longer free and open source, GNU/Linux distributions such as Debian and Fedora are no longer able to ship Redis Labs' versions of the affected modules to their users.
As a result, we have begun working together to create a set of module repositories forked from prior to the license change. We will maintain changes to these modules under their original open-source licenses, applying only free and open fixes and updates."

#### RedisLabs and Redis using the Maintainer Trademark license (monetizing the use of trademark to promote the software but staying under open source license)

RedisLabs already has a trademark license stating rules about the Redis trademark use, but they could have got financial rewards from cloud vendors about the use of the RedisLabs trademark (names, logos etc) of Redis add-ons (i.e RediSearch, Redis Graph, ReJSON, Redis-ML, Rebloom)
Instead of putting the code under commons clause, that led to give a bad signal to the user community about open source, they could have put it under the maintainers trademark license that would have kept the code open but just obliged to pay for the use of the trademark for promotion.
Cloud vendors could have resold the software but not under Redislabs Trademark about all Redis add-ons (i.e RediSearch, Redis Graph, ReJSON, Redis-ML, Rebloom)
Redis can do exactly the same about Redis Trademark to make it even more appealing about all Redis names and logos and wider in impact, keeping the code or Redis and Redislabs free for use, re-use, modification and reselling.

### Swagger versus OpenAPI Specifications (enabling indepedantly governed standards to keep their original name and branding for more independance than corporate contral )

Swagger is an API Description format that has been developed by Reverb Technologies. As human and machine readable API descriptions in JSON was needed, the project had a great success in the API community as a way to describe APIs to collaborate on it, develop against it as source of truth and open it to the world. However, Swagger format was not the core business of Reverb technologies so the maintenance of the project could not been insured matching the industry needs.
Because of that in the same time, 2 other descriptions formats have been developed by software vendors (API Blueprint and RAML), all of that lack of maintenance and velocity that led into a Specification war.
After few years of API specification war, finally a 3rd vendor Smartbear Software acquired the Swagger project trademark and code-base for a relatively important amount of money. To end the standard war and a sane industry development, SmartBear Software gave away the code base of the specification to the Linux foundation but kep the brand and trademark of Swagger (name, logo etc).
The Linux foundation renamed the project OpenAPI Specification under the OAI (Open API Initiative).
But as the name and brand is strong, everybody in the industry still call the API description format "Swagger" even it is now named OpenAPI Specification.
That leads to confusion in the industry as Smartbear continues to hardly market Swagger.io, the platform for Swagger standard tooling ecosystem and confirms the fact that the value of the trademark in an open source project.
So as result, a whole industry has converged to a standard that is on the branding beneficing to only one company, the one who owns the trademark.

#### Scenario with Swagger or OpenAPI Specifications under the Maintainer trademark license

If Swagger the  project was under the trademark license, Reverb technologies could have raised the means to sustain the project roadmap independently even if it is not their core business, instead of reselling it and needing to develop it inside the company acquiring it, what happened the year after.

#### Scenario with OpenAPI Specifications under the Maintainer trademark license

In an other way, the Linux foundation running the Open API Initiative, instead of asking sponsorship from few industry players in the OpenAPI Initiative could fund the expansion of the OpenAPI specification standard with the Maintainers trademark license.
In that way, fundings could come from all the vendors of the industry promoting their tooling ecosystem of top of the OpenAPI Specification with a trademark license instead of relying on fewer sponsors.
They would be more independent from vendors as now the OAI has in its board members its sponsors which are for the most of them API software vendors and have voting right and direct influence on the project roadmap (this is not necessarily bad or good, but it is a threat on the governance of the project)


### Docker (keeping the name and the project open source and out of corporate control)

Docker is a container engine runtime and packaging software tool that is a really popular open source project and that was designed Docker Inc. at some point there was an incompatibility between the project and the company, as the project maintenance was mostly funded by the company where a huge ecosystem was making money by directly using the project and its brand popularity.
In a defensive approach, the startup decided to rename the project from Docker into [Moby](https://blog.docker.com/2017/04/introducing-the-moby-project/) to keep the trademark inside Docker Inc. Now the community edition is called Moby and the Docker trademark is owned by the company Docker Inc. The ecosystem was not contributing enough for the startup to answer everybody's pull-request and fund the growth and the sustainability of the company.
This could have been avoided if the Maintainers Trademark license would have been applied as following.

#### Scenario with Docker with the Maintainer Trademark license

Docker Inc. adds the Trademark license to its current Docker project under Apache 2.0 license. The code stays free and Open source for all its users, nothing change for all of its users, and future versions can be implemented with continuity (that is not the case when the license update is not Free software/Open source and all current users have to change their stack and architecture because next versions will be not open source and then the code will not evolve under the OSS license they trusted the project at the beginning).
At this point, Docker Inc. can collect money from all of their ecosystem using the Docker trademark (brand, name, logo, artwork etc.) to promote commercially softwares based directly on the Docker project, or IT consultancy service around the Docker project. This money will be a reward to fund and scale the team maintaining the project and evaluate with enough resources all pull requests from the ecosystem. Also, it would be easier to open the board of the project if there is enough fund to maintain it compared that if Docker Inc has to sustain it alone.
To have a more independent way to govern the open source project, Docker Inc could have given the source code of Docker to an independent software standard organization (like Linux Foundation or any others institution like the Cloud Native Computing Foundation) who would have collected the fees to run independently and operate the sustainability of the project. An attempt had been made with the OCI (Open Container Initiative) standard where Docker Inc. and the cloud players in the CNCF worked on a standard design for containers, but the real challenge was about the name and the brand of the project, Docker.

Docker Inc would have probably as donor received a lifelong complimentary trademark license and a lifelong seat as contributor to the project allowing others to on-board the project for the best of the industry.
The project would have kept the Docker name and gathered all contributions by all the industry actors. The fact that fees are paid by the whole Industry ecosystem instead of few big brand contributors guarantees a development towards the best interest of the mass of users instead of the few supporters.
It would have benefit Docker Inc from lots of internal resources spent in maintain the Docker project to develop their own Docker Inc products and the whole industry to keep a project where Docker Inc would be associated to contribute to the whole ecosystem as they were visionaries about where the project should go, that they could continue to co-develop with the entire industry.


### MongoDB moving to Server Side Public License SSPL

MongoDB is an open source software to create and manage databases. Recently, MongoDB in the same way than Redis above decided to change their licensing model from AGPL to a new license they created the SSPL.
Their CEO Dev Ittycheria stated "Once an open source project becomes interesting or popular, it becomes too easy for the cloud vendors to capture all the value and give nothing back to the community,"
Contrary to Commons clause that is not open source, if you offer SSPL software as a service, you have to make available not only the software source code and modifications, if any, but also the source code of the applications used to run the service.
It is a extreme copyleft license, that cloud providers will probably not use as they would need to open source their whole infrastructure. The only point is that MongoDb for its Database-as-a-service product will not have to apply the SSPL license to itself as they own the IP, making it copyleft except for them.

#### MongoDB using the Maintainer Trademark license

MongoDB could have used the Maintainers' trademark license to enforce trademark fee to cloud providers about the use of MongoDB names and logo on their platform, finding a way for monetization of the $300M they invested on the software and keeping the code open-source.

### API Platform (monetizing use of the name for training and certifications)

API platform is a REST and GrapqhQL framework in PHP under MIT license. Recently some competitor software vendors made paying trainings about API platform without prior knwowledge authorization of API platform about the use of their trademark.

#### API platform case with a custom trademark license or the Maintainer trademark license

API platform could have made their own trademark policy stating like symphony or Ubuntu for example :
Restricted use that requires a trademark license
    - Any commercial use including for any services related to THE PROJECT such as providing training services, conference services, or design services (should you wish to provide such services, please contact us beforehand to explore Sensiolabs' Partnership Program);

or directly use the Maintainer trademark license that will include it directly and in a standardized way
